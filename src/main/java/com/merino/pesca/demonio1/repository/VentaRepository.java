package com.merino.pesca.demonio1.repository;

import com.merino.pesca.demonio1.model.ObjectsTO.Venta;

import org.springframework.data.repository.CrudRepository;

public interface VentaRepository extends CrudRepository<Venta,Long> {

}
package com.merino.pesca.demonio1.repository;

import com.merino.pesca.demonio1.model.ObjectsTO.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {

}

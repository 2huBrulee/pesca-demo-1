package com.merino.pesca.demonio1.repository;

import com.merino.pesca.demonio1.model.ObjectsTO.DetalleVenta;

import org.springframework.data.repository.CrudRepository;

public interface DetalleVentaRepository extends CrudRepository<DetalleVenta,Long> {

}
package com.merino.pesca.demonio1.repository;

import com.merino.pesca.demonio1.model.ObjectsTO.Producto;

import org.springframework.data.repository.CrudRepository;

public interface ProductoRepository extends CrudRepository<Producto,Integer> {

}
package com.merino.pesca.demonio1.controller;

import java.util.ArrayList;
import java.util.List;

import com.merino.pesca.demonio1.model.ObjectsTO.DetalleVenta;
import com.merino.pesca.demonio1.repository.DetalleVentaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin(origins="http://localhost:3000")
public class DetalleVentaController {

    @Autowired
    DetalleVentaRepository detalleVentaRepository;

    @GetMapping("/detalles")
    @ResponseBody
    public List<DetalleVenta> all() {
        List detallesVenta = new ArrayList<DetalleVenta>();

        for(DetalleVenta detalleVenta : detalleVentaRepository.findAll()) {
            detallesVenta.add(detalleVenta);
        }

        return detallesVenta;
    }
}
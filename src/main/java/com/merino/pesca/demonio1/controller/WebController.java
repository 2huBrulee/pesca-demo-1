package com.merino.pesca.demonio1.controller;

import java.util.ArrayList;
import java.util.List;

import com.merino.pesca.demonio1.model.ObjectsTO.Cliente;
import com.merino.pesca.demonio1.repository.ClienteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin(origins="http://localhost:3000")
public class WebController {

    @Autowired
    ClienteRepository clienteRepository;


    @RequestMapping("/save")
    public String process() {
        clienteRepository.save(new Cliente("SMASH", "Atlantis",12578457));

        return "Done";
    }

    @GetMapping("/usuarios")
    @ResponseBody
    public List<Cliente> all() {
        List clientes = new ArrayList<Cliente>();

        for(Cliente cliente : clienteRepository.findAll()) {
            clientes.add(cliente);
        }

        return clientes;
    }

    @RequestMapping("/findbyid")
    public String findById(@RequestParam("id") long id) {
        String result = "";
        result = clienteRepository.findById(id).toString();

        return result;
    }
}

package com.merino.pesca.demonio1.controller;

import java.util.ArrayList;
import java.util.List;

import com.merino.pesca.demonio1.model.ObjectsTO.Producto;
import com.merino.pesca.demonio1.repository.ProductoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins="http://localhost:3000")
public class ProductoController {

    @Autowired
    ProductoRepository productoRepository;

    @GetMapping("/productos")
    @ResponseBody
    public List<Producto> all() {
        List productos = new ArrayList<Producto>();

        for(Producto producto : productoRepository.findAll()) {
            productos.add(producto);
        }

        return productos;
    }
}
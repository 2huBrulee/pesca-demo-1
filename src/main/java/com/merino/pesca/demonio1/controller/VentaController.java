package com.merino.pesca.demonio1.controller;

import java.util.ArrayList;
import java.util.List;

import com.merino.pesca.demonio1.model.ObjectsTO.Venta;
import com.merino.pesca.demonio1.repository.VentaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins="http://localhost:3000")
public class VentaController {

    @Autowired
    VentaRepository ventaRepository;

    @GetMapping("/ventas")
    @ResponseBody
    public List<Venta> all() {
        List ventas = new ArrayList<Venta>();

        for(Venta venta : ventaRepository.findAll()) {
            ventas.add(venta);
        }

        return ventas;
    }
}
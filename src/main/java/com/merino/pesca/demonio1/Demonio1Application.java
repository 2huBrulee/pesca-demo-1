package com.merino.pesca.demonio1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class Demonio1Application {

    public static void main(String[] args) {
        SpringApplication.run(Demonio1Application.class, args);
    }
}

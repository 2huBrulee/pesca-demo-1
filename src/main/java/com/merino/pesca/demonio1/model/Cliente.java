package com.merino.pesca.demonio1.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;



public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idcliente;

    private String nombre;

    private String direccion;

    public Cliente() {
        this.nombre = "";
        this.direccion = "";
    }

    public Cliente(String nombre, String direccion) {
        this.nombre = nombre;
        this.direccion = direccion;
    }

    public Long getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(Long idcliente) {
        this.idcliente = idcliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "idcliente=" + idcliente +
                ", nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                '}';
    }
}

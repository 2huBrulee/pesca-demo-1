package com.merino.pesca.demonio1.model.ObjectsTO;

import java.util.Date;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name="venta")
@EntityListeners(AuditingEntityListener.class)
public class Venta {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idventa;
    private Date fecha;
    private String descripcion;
    private long idCliente;

    public Venta()
    {

    }

    public long getIdventa() {
        return idventa;
    }

    public void setIdventa(long idventa) {
        this.idventa = idventa;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(long idCliente) {
        this.idCliente = idCliente;
    }


}

package com.merino.pesca.demonio1.model.ObjectsTO;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;

@Entity
@Table(name="detalle_venta")
@EntityListeners(AuditingEntityListener.class)
public class DetalleVenta {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idDetalle;
    private long idProducto;
    private long idVenta;
    private Integer cantidad;
    private Double precio;

    public long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(long idProducto) {
        this.idProducto = idProducto;
    }

    public long getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(long idVenta) {
        this.idVenta = idVenta;
    }

    public Integer getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(Integer idDetalle) {
        this.idDetalle = idDetalle;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPrecio() {
        return precio;
    }

    public DetalleVenta(long idProducto, long idVenta, Integer cantidad, Double precio, Integer idDetalle) {
        this.idProducto = idProducto;
        this.idVenta = idVenta;
        this.cantidad = cantidad;
        this.precio = precio;
        this.idDetalle = idDetalle;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
    public DetalleVenta()
    {

    }



}

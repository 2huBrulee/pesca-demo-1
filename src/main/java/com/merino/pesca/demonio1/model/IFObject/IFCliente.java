package com.merino.pesca.demonio1.model.IFObject;

import com.merino.pesca.demonio1.model.ObjectsTO.Cliente;

public interface IFCliente {

    public boolean realizarVenta(Cliente cliente);

}

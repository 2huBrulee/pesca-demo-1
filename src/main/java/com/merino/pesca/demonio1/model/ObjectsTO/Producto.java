package com.merino.pesca.demonio1.model.ObjectsTO;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;



@Entity
@Table(name="producto")
@EntityListeners(AuditingEntityListener.class)
public class Producto{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer IDProducto;

    private String nombre;

    private Double precio;

    private Integer IDCategoria;

    public Producto(Integer IDProducto, String nombre, Double precio, Integer IDCategoria) {
        this.setIDCategoria(IDCategoria);
        this.setIDProducto(IDProducto);
        this.setNombre(nombre);
        this.setPrecio(precio);
    }
    public Producto()
    {

    }
    public Integer getIDProducto() {
        return IDProducto;
    }

    public void setIDProducto(Integer IDProducto) {
        this.IDProducto = IDProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Integer getIDCategoria() {
        return IDCategoria;
    }

    public void setIDCategoria(Integer IDCategoria) {
        this.IDCategoria = IDCategoria;
    }
}
